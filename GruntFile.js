// Sample grunt-jekyll grunt.js file
// https://github.com/dannygarcia/grunt-jekyll

/*global module:false*/
module.exports = function(grunt) {

    var fs = require('fs'),
        yaml = require('js-yaml'),
        strBlogTitle = grunt.option("postTitle") || "",
        strBlogBranchName = "-" + strBlogTitle.replace(/\s/g, "-").toLowerCase(),
        strBlogDate = new Date(),
        intMonth = (strBlogDate.getMonth() + 1),
        intDay = (strBlogDate.getDate()),
        strBlogFileName;

    intMonth = (intMonth < 10) ? "0" + intMonth : intMonth;
    intDay = (intDay < 10) ? "0" + intDay : intDay;
    strBlogDate = strBlogDate.getFullYear() + "-" + intMonth + "-" +  intDay;

    strBlogFileName = strBlogDate + strBlogBranchName + ".md";

    // Project configuration.
    grunt.initConfig({
        clean : {
            before : ['./_site', './_temp', './static'],
            after  : ['./_temp']
        },
        concat: {
            dist: {
                src: ['_static/js/libs/jquery.js', '_static/js/libs/bootstrap.js', '_static/js/libs/jqcloud.js', '_temp/js/externalmain.js'],//note _temp for external main
                dest: '_temp/js/optimized.js'
            }
        },
        copy : {
            all : {
                files : [
                    //copy files no matter what environment
                    {
                        expand: true,
                        cwd: '_static/',
                        src: [
                            '**',
                            '!css/bootstrap*',
                            '!css/buttons.css',
                            '!css/canddiWebsite.css',
                            '!css/footer.css',
                            '!css/loginModal.css',
                            '!css/module_blog.css',
                            '!css/jqcloud.css',
                            '!css/navbar.css',
                            '!css/rightHandSide.css',
                            '!css/specificBlog.css',
                            '!css/specificPage.css',
                            '!js/**',
                            '!js/optimized.js'
                        ],
                        dest: 'static/'
                    }
                ]
            },
            blogPost : {
                files : [
                    {
                        src : ['_blogTemplate.md'],
                        dest : "./_temp/" + strBlogFileName
                    }
                ]
            },
            dev : {},
            prod : {}
        },
        gitcommit : {
            blogpost : {
                options : {
                    message : "New blog post " + strBlogDate
                },
                files : {
                    src : ['_posts/*', 'images/blog/*', 'images/blog/**']
                }
            }
        },
        gitcheckout : {
            blogpost : {
                options : {
                    branch: 'blogpost' + strBlogBranchName,
                    create: true,
                    override: false
                }
            },
            develop : {
                options : {
                    branch: 'develop',
                    create: false,
                    override: false
                }
            },
            master : {
                options : {
                    branch: 'master',
                    create: false,
                    override: false
                }
            }
        },
        gitpull : {
            develop : {
                options : {
                    branch: 'develop',
                    remote: 'upstream'
                }
            },
            master : {
                options : {
                    branch: 'master',
                    remote: 'upstream'
                }
            }
        },
        gitpush : {
            current : {
                options : {
                    remote: 'origin'
                }
            }
        },
        htmlmin: {
            prod: {
                options: {
                    removeComments: true,
                    removeEmptyAttributes : true,
                    collapseWhitespace: true
                },
                files:[
                    {expand: true, cwd: '_site/', src: ['**/*.html', '*.html', "!static/docs/**"], dest: '_site/'}
                ]
            }
        },
        jekyll: {
            dev: {
                options : {
                    drafts : true,
                    serve: true,
                    watch : true
                }
            },
            watches : {
                options : {
                    drafts : true,
                    watch : true
                }
            },
            prod: {
                options : {
                    config: '_config-live.yml'
                }
            }
        },
        jshint : {
            files : {
                src : ['_temp/js/externalmain.js']
            }
        },
        mkdir: {
            site: {
                options: {
                    create: ['./_site']
                }
            }
        },
        'string-replace': {
            blogPost : {
                files : [
                    {
                        src : ["./_temp/" + strBlogFileName],
                        dest: "./_posts/" + strBlogFileName
                    }
                ],
                options : {
                    replacements: [
                        { pattern: /POST_TITLE/g, replacement: strBlogTitle },
                        { pattern: /POST_DATE/g, replacement: strBlogDate + " 00:00:00" }
                    ]
                }
            }
        }
    });

    //register tasks - these are what get called from command line
    grunt.registerTask('newPost', ['_validateNewPost', 'clean:before', 'gitcheckout:develop', 'gitpull:develop', 'gitcheckout:blogpost', 'copy:blogPost', 'string-replace:blogPost', 'buildDevWatch']);
    grunt.registerTask('masterBuildLive', ['gitcheckout:master', 'gitpull:master', 'buildLive']);
    grunt.registerTask('pushPost', ['gitcommit:blogpost', 'gitpush:current']);

    // plugin tasks
    grunt.loadNpmTasks('grunt-mkdir');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-csslint');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-htmlmin');
    grunt.loadNpmTasks('grunt-jekyll');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-md5');
    grunt.loadNpmTasks('grunt-string-replace');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-git');

//    grunt.registerTask('_validateNewPost', 'Validate we got a post title', function() {
//        if (!strBlogTitle) {
//            grunt.log.error('grunt newPost requires --postTitle="Blog title" to be passed');
//            return false;
//        }
//
//        grunt.log.writeln('Initial validation successful');
//    });
//
//
//    /**
//     * Here we validate the related post on each blog article is
//     * 1. Lowecased
//     * 2. In the categories array
//     *
//     * (note we don't error if there's no related post so we can support older blog posts)
//     */
//    grunt.registerTask('_validateRelatedPosts', 'Validate the related post category for each blog post', function() {
//        var arrPosts = grunt.file.expand("./_posts/*"),
//            bFail = false;
//
//        //loop through all our blog posts
//        arrPosts.forEach(function (strPath) {
//
//            //if we've already failed - pointless to load the file
//            if (bFail) {
//                return;
//            }
//
//            var file = grunt.file.read(strPath),
//                objYaml,
//                bFound = false;
//
//            //parse yaml section of the post
//            objYaml = yaml.load(file.substring(3, file.lastIndexOf("---")));
//
//            //this shouldn't happen - but just incase
//            if (!objYaml) {
//                grunt.log.error("Post appears to have no meta data: " + strPath);
//                bFail = true;
//                return;
//            }
//
//            //if we have no related category - meh, don't care
//            if (!objYaml.related) {
//                return;
//            }
//
//            //check related category is lower cased
//            if (objYaml.related.toLowerCase() !== objYaml.related) {
//                grunt.log.error("Related category must be lowercase: " + strPath);
//                bFail = true;
//                return;
//            }
//
//            //check the related category is in the categories array
//            //we use this rather than indexOf so we can ignore cases
//            objYaml.categories.forEach(function (strCategory) {
//                strCategory = strCategory.toLowerCase();
//
//                if (strCategory === objYaml.related) {
//                    bFound = true;
//                }
//            });
//
//            //if we didn't find the related category - throw error
//            if (!bFound) {
//                grunt.log.error("Related category not in categories array: " + strPath);
//                bFail = true;
//            }
//        });
//
//        //if we failed - return false
//        if (bFail) {
//            return false;
//        }
//
//
//        grunt.log.writeln('Post validation successful');
//    });
//
//
//    /**
//     * This task validates our live build. For the moment all it does is validate we haven't
//     * got auth.canddi.local and have got auth.canddi.com instead in our externalmain.js
//     */
//    grunt.registerTask('_validateLiveBuild', 'Validate our live build is not a dev build', function() {
//
//        var strFile = grunt.file.read("." + strPathBuildJS);
//
//        if (-1 !== strFile.indexOf("auth.canddi.local")) {
//            grunt.log.error("Live Javascript contains auth.canddi.local");
//            return false;
//        }
//
//        if (-1 === strFile.indexOf("auth.canddi.com")) {
//            grunt.log.error("Live Javascript does not contain auth.canddi.com");
//            return false;
//        }
//
//        grunt.log.writeln('Validation of live build successful');
//    });
};