---
layout: coverLetter
title: Curriculum Vitae
dear: "Mr Thompson"
company: "Bede"
bFaithfully : true
date: '09-07-2014'
companyAddress : 'Bede Gaming<br/>1 Moor Road South<br/>Newcastle upon Tyne<br/>NE3 1NN'
permalink: '/bede/index.html'
---

I am writing to put myself forward for the role of JavaScript Developer as advertised on your website.
As you can see from my CV, I have many skills pertinent to this role gained both from university and from my time in my current
position.

I am now looking for new challenges in a larger company having spent the past two years working for a small startup company in
Newcastle. This role has enabled me to experience a large amount in a very short space of time and has given me a wealth of experience
and responsibilities which I feel will benefit me in any future roles.

I believe that I am an ideal candidate for this role because I have considerable experience both with Marionette and and node as well
as experience with Grunt. While I haven't had any direct contact with the other technologies you list (Karma, Engine.io and CreateJS)
I am confident that I will be able to learn these quickly. I also try to keep up to date with current web technologies including
HTML5 and CSS3 both inside and outside the office.

In fact during my final year at university I took the opportunity during my graphics module to teach myself webGL having
been given the option to undertake the coursework in any language of my choice. Outside of work I also maintain my own 
node/angular project for my smart home. Sadly this has to remain closed source due to an NDA I signed with the company behind
my smart light switches. I also maintain a personal blog built using the jekyll site generator. In fact this cv has been written using
HTML, Markdown, Liquid and CSS and is available to view online at skysteve.com/about/cv/bede/ .

In my time in my current position I have been responsible for developing most of a large marionette based application which
also uses mustache for templates. Along with this I have been responsible for all three of the node applications within the business
and maintaining and extending our client side JavaScript (which must be IE6 compliant).

I am confident that I possess the attributes you’re looking for and that my experiences gained in my current role would provide invaluable 
insight and capability to Bede Gaming. I also feel that I would be a good fit culturally for the business having read
through your website including the meet the team page (I also went to university with one of the team).

I feel that Bede gaming would be the perfect next step for me in my carer as a web developer because it will provide me
with an exciting, challenging and interesting place to work alongside some very talented and interesting people. I also believe
that the services Bede create will offer me space to learn and develop and apply my skills very early on. 
 
I would welcome the opportunity to discuss my application in person.
