---
layout: default
title: About Me
description: "Short description about my life so far detailing how I came to be where I am today."
---
<br/>
<div class="large-70 medium-70 small-100">

### Personal

I'm Steve aka SkySteve a.k.a SkySte (I thought I'd upgrade to SkySteve around age 21)
I'm currently a <script>document.write(new Date().getFullYear() - new Date(1990,02,16).getFullYear())</script> year old web
developer living in Newcastle Upon Tyne working for [Spontly][1] . Originally
from the South of Isle of Man I moved to the UK for Uni in 2008, graduated from Newcastle University,
found a job new Newcastle and stayed!

I've been a keen paraglider pilot since I was 15, and a keen kitesurfer since April 2013. Outside of these sports I'm starting
to get quite into cycling now it's spring again. I've also dabbled in skydiving, climbing,
skiing and all the usual sports like swimming, rugby etc. I have really no interest in football/cricket/rugby but as of
2013 I have found myself really enjoying American football! Just need to settle on a team to support for the coming season,
at the moment it's between the 49ers and the Dolphins.

Since around age 14/15 I've had the idea that I want to move to America/Canada at some point after falling in love
with both places when I went to visit. Now I'm <script>document.write(new Date().getFullYear() - new Date(1990,02,16).getFullYear())</script>
and I've left home, finished uni and been working for a over 1.5 years I'm starting to put some effort into making 
that dream a reality. Ideally I'd love to move and continue working for small tech startups as I have been really 
enjoying my time so far in the startup community. Ideally I'd like
to start my own business too but one step at a time, moving is my focus at the moment.


### Work

Work wise I suppose I'm paid to be a JavaScript developer, something I didn't ever imagine doing back when I
was 15/16 dreaming of being an airline pilot but I love my job. I spend most of my working week playing with a mixture of
Backbone/Marionette, raw IE6 compliant JavaScript, Node.js, Mustache, HTML, CSS, MongoDB and the odd bits on the side.

While at Newcastle University I spent a year working in industry for Hewlett Packard down in Bristol as part of the
deduplication team in the disk to disk backup business. While this was mostly a supporting role I learnt a hell of a lot
during my time at HP and loved every minute of it and all the people I met along the way,

When I returned to University for my final year I worked on a web based project in Java EE coupled with JavaScript on the
frontend to make a paragliding recommendation tool. The aim was to pull in a whole bunch of weather data, trends and pilot
updates to recommend the best place to go flying at any given time. While the project did work, I haven't had the time to maintain
it and given everything I've learnt since starting work there's a lot I'd do differently now!

After University I had the summer to relax then started working for CANDDi in September 2012 where I've been ever since
despite lots of changes around me. As I've said I spend most of my time writing JavaScript and consequently most of my
personal pet projects tend to end up in JavaScript of some form. Be that Node.js, Angular, Backbone .... Although I
do like to keep my eye in with Java by maintaining my LightwaveRF android app.

While I work mostly with Backbone and Marionette I do try to keep up with other frameworks, libraries and frontend web
technologies. When I get the time for personal projects I try to look outside Backbone just to keep up to speed with what
else is around. I also obviously maintain this site using [jekyll][2] which is all [open sourced][3].

### Other bits

Aside from sports and work in the past year or so it seems I've become somewhat obsessed with graphing my life.
I'll get round to writing that up as a blog post at some point, but for now you can find my last.fm, fitbit and strava
stats below. Again when I get the time I'll work out a smart way to upload my home energy graphs. I guess this
obsession really started a few years back with logging all my paragliding flights on a GPS and then grew as I
discovered fitbit...

Oh and finally, my last little obsession in the past year of so since I got my own appartment has been home
automation. This basically boils down to <a href="/random/2014/01/11/life-with-lightwave.html">LightwaveRF</a>
sockets and light switches with the addition of some custom code. The system has grown from the standard
LightwaveRF app to include things like geofences to turn
on sonos when I'm near home, and turn everything off when I leave it! Again there's bound to be a few blog
posts on this topic over time.
</div>
<div class="large-10 medium-20"></div>
<div class="large-20 medium-20 small-100">
    <img
            id="profilePic"
            src="{{site.data.links.profilepic}}">
    <p style="text-align: center">Steve Jenkins
</div> 

[1]: {{site.data.links.spontly}}
[2]: http://jekyllrb.com/
[3]: https://github.com/skysteve/skysteve.github.io
