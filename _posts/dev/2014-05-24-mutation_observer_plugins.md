---
layout: post
title:  "Controlling plugins with MutationObserver"
date:   2014-05-24 13:15:00
category: dev
tags : ["javascript", "dev", "mutation_observer", "mutation_plugins"]
code : true

img:
    alt: "canddi capture on skysteve.com"
    caption: "CANDDi Capture reformatted on this page using MutationObserver"
    styleCaption: "top: -4px"
    url: "/img/posts/dev/2014/05/skysteveQuestion.jpg"

summary: "MutationObserver provides a powerful tool to intercept and restyle plugins you may be running on your site. Here I'll break
down how I use it to restyle CANDDi capture on this site."
tease: "Redesign and control third party plugins using MutationObserver"
---
In my last post I wrote about how you can use MutationObserver to detect DOM changes. That post didn't really give any
practical applications on how and why you'd want to use a MutationObserver. As I see it the most likely use of MutationObserver
is for use in browser extensions. For example you may build a chrome extension which watches for changes on facebook and
then strips out posts that match certain filters.

However another nice use is to use MutationObserver to intercept and re-format plugins you may use on your site. For example I run
[CANDDi][1] analytics on this site (as an employee of course I would). One of the features of CANDDi is called CANDDi
capture. This feature allows you to show questions to visitors who meet certain criteria. For example I have a question
on this site which is only shown to people who have read my about me page. I won't go into the details of how this process works,
please check out the link above or leave a comment below if you'd like more information about [CANDDi][1].

CANDDi runs on many client's websites, as a result we needed to have a way to integrate questions without affecting client's pages
as much as possible. The solution we came up with was to display the question in the bottom right of the page.

<figure class="vspace">
    <img src="/img/posts/dev/2014/05/motorsQuestions.jpg" alt="canddi capture on canddi motors" title="Canddi Capture">
    <figcaption class="dark" style="top: -16px">
        <p>CANDDi capture on our test site</p>
    </figcaption>
</figure>


This works well for most people, it's quick and easy to integrate and you can customise the colours to better match your branding.
But it is possible to use MutationObserver to detect when CANDDi adds a question to your page (because remember it may not
necessarily show if visitors don't match certain criteria). This lets you then re-style the question into something which
integrates tighter with your page.

<figure class="vspace">
    <img src="/img/posts/dev/2014/05/skysteveQuestion.jpg" alt="canddi capture on skysteve.com" title="Canddi Capture">
    <figcaption class="dark" style="top: -4px">
        <p>CANDDi capture integrated to the bottom of this page</p>
    </figcaption>
</figure>

As you can see, this question now tightly integrates with the page, it seems to extend the links bar at the bottom of the
page so that the question appears part of the page rather than an added extra in the bottom right hand corner.

This trick can be applied to almost any plugin if you take the time to work out how the plugin is structured and how you
can dismantle it and re-assemble it into what you need.

## The Reformatting process
There are 3 stages to being able to achieve what you see above. Observing when the plugin has loaded, deconstructing what
the plugin has added to the page and finally re-assembling the parts you need.

### Step 1 - Observing the load
Step one is basically to implement the code in my [previous post] [2]. This will let you know when anything is added to the dom.
Depending on the plugin and what you want to do you may need to observe the whole page, or you may be able to observe a specific
div. You may need this observation code to be in the header so you're observing the page as it loads, or in the footer so you only
observe everything that loads after your page is rendered. Start with observing as little as as specific as you can (this is the
least expensive option). If you can't get what you need from the late/specific observation, gradually move your observation
code higher up.

{% highlight javascript linenos %}
if (window.MutationObserver) {
    //now create our observer and get our target element
    var observer = new MutationObserver(fnCallback),
            elTarget = document,
            objConfig = {
                childList: true,
                subtree: true,
                attributes: false,
                characterData: false
            };

    //then actually do some observing
    observer.observe(elTarget, objConfig);
}
{% endhighlight %}

For me the code above runs at the very bottom of the page and observes the whole document because I wanted to intercept the
JavaScript that CANDDi loads as well as the html it adds to the page.

## Step 2 - Deconstructing the plugin
Most of step 2 is actually jumping into developer tools in your browser and working out how the plguin is constructed

* What HTML makes up the view?
* How is it styled?
    * Is the style an attribute on the elements?
    * What CSS files are loaded?
    * What class names/ids are used?
* Are there any hidden elements for transmitting data?
* What parts of the view do you need/don't need?
    * What are their class names/ids?
    * How are they nested?


## Step 3 - Reconstruct the plugin
Now we get back to code. You'll want a callback function for your MutationObserver which looks for the class names and ids
you identified above and strips out the ones you don't want. That's the easy bit, getting rid of elements you know you don't
want or need cleans up the code you have left to work with. BUT it will likely mess up the styling of everything around them
so you'll need to fix that by adding some css.

Your choices with CSS are either to add your own classes to the elements and add these to your CSS file(s) you already load
on the page, or to add style attributes to the elements. Here just do whatever you feel is easiest to manage, personally I went
with adding style attributes because it kept all the code in one place, but as I write this part of me wishes I'd gone down
the class route (I might refactor that when I get a chance).

Rather than copying all the code for restyling questions into this post, below is snapshot of what's going on.

{% highlight javascript linenos %}
var canQs = document.querySelector("#CQ-canddi");

if (canQs) {
    //shrink text area height and change the prepopulated value to a placeholder attribute
    var ta = canQs.querySelector("textarea");
    ta.setAttribute("rows", "1");
    ta.removeAttribute("cols");
    ta.setAttribute("style", "width:65%; margin-right:10px; ...");
    ta.setAttribute("placeholder", ta.value);
    ta.value = "";
}
{% endhighlight %}

The code above is what I use to reformat a large text area down to one long line and add some style to move it around
in the page (I've shortened the style so it fits on one line)

## Final Thoughts
Above you can see how you can use MutationObserver practically to reformat plugins you may be running on your site. I have
used the example of CANDDi capture but I could just as easily apply this to other plugins.

This post started out as a little experiment to see if I could make questions on my site integrate more tightly and having
done so I though I'd write it up. There may be nicer/easier ways to achieve the same effect, for example you could just use
the same endpoints and re-write the html for your plugins manually. It depends on the plugin and what you want to achieve,
in my case doing this would mean I'd have to mange showing the question to the right visitors where as by using MutationObserver
I still let the plugin handle that and I just handle the styling.

Comments and thoughts much appreciated.

[1]: {{site.data.links.canddi}}
[2]: /dev/2014/04/01/detecting_dom_changes.html
