---
layout: post
title:  "LightwaveRF API - Developing with LightwaveRF"
date:   2014-01-19 12:00:00
category: dev
tags : ["lightwave", "dev"]

tease: "Developing with LightwaveRF"
summary: "LightwaveRF has a relatively simple API allowing the control of all areas of the system. This post is a
quick into into the API without disclosing it due to legal constraints explained below."

img:
    alt: "screenshot of the dashboard I built with the LightwaveRF API"
    caption: "The result of my efforts using the LightwaveRF API to make a nicer web interface"
    url: "/img/posts/dev/2014/01/lightwaveAPI.jpg"
---

## Accessing the API
In order to gain access to the LightwaveRF API you need to complete an [application form][1]. You will then be
emailed a pdf document containing details of the available commands. It asks for a reason why you want access and
I simply said a personal project and was emailed the pdf the next day. At the end of the application form it asks you
to sign to say you won't disclose the API, because I've signed it I'll just give a broad overview here rather than
any specifics.

I'm sure the company have a good reason for wanting you to request access, but personally this seems very limiting.
Especially given that other companies in this space such as Smart Things openly publish their API. In my opinion this
is part of the reason why smart things seems to ingrate with lots of new products while I'm yet to see anything
integrate with LightwaveRF which seems a shame. While this is only a personal opinion and I'd love to be proved
wrong, I get the feeling that unless LightwaveRF steps up and opens up it'll have a hard time competing when Smart
Things branch outside of the USA but I digress.

## API Overview
In order to use the API you're going to need a [wifi link][2] connected to your home network. At this point I should
say that the wifi link has two ways to interact with it. It tries to hold open a connection to a central server
which drives the standard application. While you could attempt to intercept HTTP requests and decode this API and
send your requests to the central server and get them sent back to your device, this is very inefficient.

The best way to connect to the wifi link is via the local interface which is detailed in the API document. This API
runs over UDP by sending ASCII packet commands to the wifi link over the local network. One point to note though
is that the wifi link will only accept commands from known devices. If you try to send commands from an unrecognised
device it'll ask you to confirm the device by pressing a button on the box. After this confirmation the paired device
will just work. However there is a limit on the number of devices you can pair, I believe this is 6, but I'm not certain
on that.

The API will allow you to send commands to:

* Devices
    * Turn Devices on/off
    * Lock/Unlock devices
    * Set dim levels on supported devices
    * Turn ALL devices OFF (note there is no all on) per room
* Moods (I'll not go into detail here as I don't use them)
    * Save a mood
    * Recall a mood
* Timers
    * Set a timer
    * Remove a Timer
* Sequences
    * store a sequence
    * Run a sequence
    * Cancel a running sequence
    * Delete a sequence
* Sequences AND timers
    * Cancel all sequences AND timers
    * Delete all sequences AND timers
* Other
    * Query the energy monitor (if available)
    * Set the timezone
    * Set your location (used for sunrise/sunset timers)

## Limitations
In my opinion there are two sets of limitations, firstly there's limitations due to the underlying hardware,
secondly there are limitations of the API.

Lets start with the hardware, by far the most annoying thing is the fact you can't query a device's state. I.e. you
can't ask a device if it's on/off locked/unlocked or it's dim level. Therefore if you want to be able to record this
information everything has to flow through your own system and you have to manually record the device's state when it
changes. But that's not as easy as it sounds, related to the fact you can't query a device's state, you also can't tell
if the command has completed at an API level. When you send a command the wifi link will reply to tell you if it has
sent the command to the relevant device(s) but won't tell you if the device state changed.

For example I have a socket at the other end of my flat, which 99% of the time I ask it to turn on it'll work,
however sometimes it doesn't because of the range. At an API level there is no way to say "that didn't work,
try again". So you may record that a light is on because you sent the on command, but in reality nothing happened.
And that's without assuming from time to time you're going to use the actual light switch.

Secondly the API limitations, here the most annoying thing is the lack of any GET APIs. For example if you store a
sequence using the standard app, you can't query the wifi link to list the available sequences, you have to re-create
them again. Which is fine, except then you're tied into your own system rather than being able to flip between the
two. Related to this is the fact you can only store a maximum of 32 sequences AND timers. Not 32 of each,
32 combined. And to call or remove one you need to reference it by name, which means you'll need to store that
somewhere, which seems silly when the wifi link already knows about it and you could just query it from there.

## Devices
At an API level all devices are referenced by a device ID and a room ID. For example the light in my kitchen has a
room ID of 4 and a device ID of 1. A device can be paired to I think 6 "things" i.e. your light may be paired to a
remote control, the wifi link in room 1 and a PIR sensor. This would be 3 pairings,
you could also assign your light to two rooms if they overlap. For example my kitchen light is also part of my
livingroom because the switch for the kitchen light is actually in the livingroom. This counts as a fourth pairing.
In order to assign names to a device you'll need to store these manually and link them to device and room IDs.

## Sequences and Timers
Sequences and timers are very similar things at an API level so I'll group them together. A sequence is basically
just a list of commands you can store and name. You provide the API with a room and device ID as before,
but this time you also supply a delay period before executing the next command. The recommended minimum delay is 3
seconds between commands to give each command time to find it's way from the wifi link to the device. Sequences are
referenced by a name which must be unique between sequences AND timers.

Timers are as the name suggests a way of telling the system to do something at a specific time. Timers may control a
single device, or run a pre-stored sequence. For example in the screenshot above, I have a timer to run my "Sonos On"
sequence every week day morning to wake me up. Timers are actually pretty smart, along with being able to customise
the days and time the timer will run, you can also setup "special" timers. These are timers which don't have an exact
fixed time but will run based on sunrise/sunset. The best example of this I can think of is if you have automatic
curtains and want them to open at sunrise to let the sun in to warm your home and then close them again when it
starts to get dark to keep the sun out. You can also run these special sequences at a period before/after
sunrise/sunset such as 1 hour before sunrise.

One thing to note as I said above is that timers and sequences must have unique names and you can only store a
maximum of 32 of them.

## Energy Monitoring
Where LightwaveRF is great is that it has energy monitoring built into it. The energy monitoring query is quite
simple. When you ask for the energy monitor data you get back the current usage, the maximum usage for today,
the total usage for today and the total usage for yesterday. Which is nice, but if you want to start doing historic
trends (I'll write that up as it's own blog post) you need to poll the energy monitor and store what you need.
Personally I don't really care about the maximum value, it's always roughly the same because it's always when my
boiler kicks in. But the current usage and total values are all handy, especially the total yesterday because it
saves you messing about catching the total today at the end of the day. One point to note here is that energy data
changes every 30 seconds, so it's not worth polling it more frequently than that.

## Closing words
And that's my brief introduction into what you can/can't do with the LightwaveRF API. In a future blog post I'll go
into a bit more about what I've done on top of this API to add more value to my system than the standard apps provide.


 [1]: http://help.lightwaverf.com/knowledgebase.php?article=15
 [2]: http://www.lightwaverf.com/store/home-automation-controls/smart-homes-wifi-link

