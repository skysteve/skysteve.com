---
layout: post
title:  "Ignoring semalt in Google Analytics"
date:   2014-05-10 14:45:00
category: dev
tags : ["javascript", "dev", "semalt"]
code : true

tease: "Is Semalt distracting in your analytics"
summary: "Around April 2014 I began to notice semalt appearing in my analytics tools. It doesn't appear to be disruptive
but it is annoying"

img:
    alt: "Semalt in Google Analytics"
    caption: "Semalt traffic in Google Analytics"
    url: "/img/posts/dev/2014/05/semalt.png"
---

### What is Semalt?
Semalt seems to be a service claiming to offer you overview of your page ranking in Google around the world. It also
seems to offer some basic insights into potential issues on your page. Though [Google PageSpeed Insights][1] is arguably
a better tool.

Semalt also seems to offer the ability to "spy" on your competitors. I honestly have no idea who or why, but around April
2014 someone seems to have listed my site as a competitor. As a result on an almost daily basis I began to see referrers
on my website from semalt.semalt.com/crawler.php.

### Is Semalt dangerous?
Semalt has no ability to affect your page rankings, your content or disrupt your site in any other way. It appears all it
does it search Google for the keywords and URLs you give it, and look for yours and your competitors' sites and some
basic insights into the structure and content of the page.

Semalt is more annoying that anything because it skews your analytics and potentially gives false positives for conversions
etc on your site. Fortunately it's fairly easy to stop semalt from showing up in your analytics.

### How to block Semalt
There are a couple of ways you can block semalt. Sadly they don't seem to respect robots.txt files despite being a crawler
service which means you need to be a bit more proactive. It also seems to use common browser user agents so you can't
block by user agent. The only thing that gives it away is the referrer.

#### Server side
The first way you can block semalt is server side. If you're using apache web server you can add the following to either
your .htaccess file or config file(s) to stop semalt from being able to "see" your pages.

{% highlight apache %}
RewriteEngine on
RewriteCond %{HTTP_REFERER} semalt\.com [NC]
RewriteRule .* - [F]
{% endhighlight %}

#### Client side
There are some occasions where you can't block access server side. This is normally because your hosting company doesn't
allow you access to the server config and doesn't let you create an .htaccess file within your site. Fortunately it's
quite easy to stop semalt appearing in your analytics. Rather than blocking access to your page, this method just stops
Google Analytics from loading.

The code below will check if the referrer for this page was semalt.com (at some point in the future they may get wise to
this and use another domain).

{% highlight javascript %}
<script type="text/javascript">
    var bNoAnalytics = false;
    if (document.referrer && /semalt\.com/i.test(document.referrer)) {
        bNoAnalytics = true;
    }
</script>
{% endhighlight %}

You can then adjust your google analytics code to not load if the flag set above is true.
{% highlight javascript %}
if (!bNoAnalytics) {
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-3456789-1', 'yourdomain.com');
    ga('send', 'pageview');
}
{% endhighlight %}

If you wanted to be cheeky too you can add a little message to the person who may be tracking you (I'm not sure if they can
actually see this but I like to think they can).

{% highlight javascript %}
if (bNoAnalytics) {
    document.body.innerHTML = "Hello Semalt user - please stop tracking me";
}
{% endhighlight %}
(Note that this message needs to be the last thing on the page so it overrides all the body text so all semalt can screenshot
is your message).


#### Updating your Google Analytics code
To make life simpler I've created a little tool below. Simply paste in your existing google analytics code, and it'll
wrap it up to ignore semalt. (You can also use this tool to wrap up other analytics such as [CANDDi][2]

<script type="text/javascript">
    function fixGA() {
        if (!document.querySelector) {
            alert("Unfortunately this tool requires you to use a modern browser, please upgrade your browser to use this tool.");
        }

        var txtGA = document.querySelector("#txtGA"),
            txtGANew = document.querySelector("#txtGANew"),
            divNewCode = document.querySelector("#divNewCode"),
            originalTxt = txtGA.value.trim(),
            scriptTagStart = "",
            scriptTagEnd = "";

        if (!originalTxt) {
            alert("please enter your existing code");
        }

        if ("<script" === originalTxt.substring(0, 7)) {
            scriptTagStart = originalTxt.substring(0, originalTxt.indexOf(">")+1) + "\n";
            originalTxt = originalTxt.replace(scriptTagStart, "");

            scriptTagEnd = "\n</scr" + "ipt>";

            originalTxt = originalTxt.replace(scriptTagEnd, "");
        }

        originalTxt = "if (!(document.referrer && /semalt\.com/i.test(document.referrer))) {\n" + originalTxt + "\n}";

        txtGANew.value = scriptTagStart + originalTxt + scriptTagEnd;
        divNewCode.setAttribute("style", "");
    }
</script>

<form class="ink-form" onsubmit="return false">
    <div class="control-group required">
        <label for="email">Existing Analytics code</label>
        <div class="control">
            <textarea id="txtGA" required="required" rows="8"></textarea>
        </div>
    </div>
    <button id="btnFixGA" class="ink-button green" onclick="fixGA()">Generate Code</button>
    <div class="control-group" id="divNewCode" style="display:none;">
        <label for="email">New Analytics code</label>
        <div class="control">
            <textarea id="txtGANew" required="required" rows="8"></textarea>
        </div>
        <p class="tip">Replacing your existing code with the code above</p>
    </div>
</form>



### Other annoying services
If you notice any other annoying services you'd like to ignore please let me know and I'll update the tool above to ignore
those as well.

[1]: https://developers.google.com/speed/pagespeed/insights/
[2]: http://canddi.com