---
layout: post
title:  "Step Away from the code! The power of taking a break"
date:   2014-06-06 22:00:00
category: dev
tags : ["dev", "step_away"]

tease: "Solving problems by stepping away"
summary: "As developers we spend a lot of our time staring at code wondering either why it won't work the way we want or
why it's suddenly broken. There's a lot of evidence to suggest stepping away from the code might be much more productive
than staring at it for extended periods."

img:
    alt: "Confused coder"
    caption: "Confused coder needs to step away!"
    url: "/img/posts/dev/2014/06/confused.jpg"
    styleCaption: "width:477px;"
---

As a developer obviously I enjoy problem solving (if I didn't I'd have a different job by now). Having a problem in front 
of me that when it's first described I think "How the hell am I going to do that!?" is what makes it an interesting job.
The same goes for bugs that you never expected to occur.

Sometimes these problems are obvious, "oh that line should of said true instead of false" for example, other times you can
spend literally hours staring at code and get nowhere only to find that indeed that line should of said true. I know,
I've done it! And sadly more than once (maybe not quite true = false but probably not too far off).

It's frustrating, you carefully read every line of code over and over, you replay the scenario over and over again and 
sometimes the bug happens, sometimes it doesn't and you're stumped! So what can you do other than read over that code again?

## Take a break!
It may sound counter intuitive but stepping away from the screen frees up your brain to wander. This goes for many things
not just coding by the way! Struggling to think of what you're going to write your next blog post about? Or even a good example
for this paragraph? Stop! Do something else!

At work we have a phrase "the inspiration wee". This is literally getting up, going to the toilet and coming back (assuming
you need a wee in the first place of course!). What's funny is it's amazing how many times this actually works! Between us
we'll have a problem and stare at it, discuss it.... then go away for 2 minutes and suddenly BOOM! In mid flow, there's the
answer! Of course! That's what wrong, and that's how I fix it!

Ever noticed how when you're drifting off to sleep you have most of your good ideas? Or while you're driving down the motorway
you'll realise how to do/fix something? The technique works when you basically step away from your problem and don't think
about it for a while. Whether that's going for a wee, going to make a coffee, juggling, whatever it is (almost).

## The science bit
There is real science behind this! As well as coding I love a bit of psychology (it's fascinating) and one day while watching
something on TV this subject came up. On the show they conducted an experiment with 3 people, asking each to name as many
uses as they could for a standard brick (like you'd use to build a house). It genuinely didn't matter how many uses they
came up with, this was only part one.

Next they gave each subject a different task to do for around 2 minutes.

1. The first person simply had to sit in silence and do nothing
2. The second person had to sort a collection of lego bricks into various colours
3. The final person had to build a house from lego bricks

After the time was up each person was asked to name as many new uses of a brick they could think of. Now you're probably thinking
the person who did nothing had time to think and would easily think of more new uses? Wrong! The answer is person 2. (I forget
the actual numbers now, again they're not that important.) The person who had a menial task of sorting bricks "won" this task
and by a sizable margin!

The theory here says that by doing something different and allowing "the front" of your brain to focus on the task in front of
you, in this case sorting bricks, going to the toilet, making a coffee.... that your more creative brain is free to pull
lots of information together in the background and make connections your logical brain hasn't been able to make.
Essentially this is like asking another person to take a look at your code and work out what's wrong!

See here for the really [detailed/scientific study][1].

## Final words
So there you have it, next time you have a problem you can't solve and find yourself stuck on where to look next, step away,
go and make a coffee, go for lunch... Just make sure you've done some investigation so your mind has some data to work with.
And make sure the task you're doing on your break doesn't require too much effort so your brain has some capacity to think.

Sadly this technique doesn't always work, but personally I find it works more often than it fails.

[1]: http://www.pnas.org/content/106/21/8719.abstract?sid=80b6f8af-73b6-4791-b033-a5c8625a7c62

