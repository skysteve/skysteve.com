---
layout: post
title:  "Writing a CV as a developer"
date:   2014-08-14 22:00:00
category: dev
tags : ["dev", "writing_cv"]

tease: "Use HTML not word to make your CV"
summary: "As a frontend developer I spend my time writing (among other things) HTML so when it comes to writing a CV why use
Microsoft work/Apple pages when you can use your CV as an excuse to show off your skills?"

img:
    alt: "CV screenshot"
    caption: "Screenshot of my online CV"
    url: "/img/posts/dev/2014/08/cv_screenshot.png"
---

We've almost all written a Curriculum Vitae (CV) at some point in our lives and I'd bet that most people would open up Microsoft word,
Apple pages, Google docs... and start writing or worse still use a predefined template when writing their CV. Having had to
sift through piles of CVs before I can assure you that if your CV looks the same as everyone else's it's boring. Where as if it 
has a unique layout it's more attractive to read (thus giving you a much better chance of success).

Most people aren't fortunate enough to be able to code so programs above are all they have to work with, but as developers
we have better tools to craft the perfect CV to show off our skills. If you're writing a CV stating your skills as HTML
and CSS, what better way to demonstrate them than actually using them to make your CV?

## Getting started
My advice in getting started is more or less the same as you'd read anywhere else when it comes to writing a CV. Make a list
of all the things you've done in the last few years and your relevant skills/qualifications. Put them on paper or even put them
in a text file somewhere. 

Then write the content like you would any other HTML file you'd write and style it to look good on a screen, after all if
it looks awful you'll never get a job! If you want to be fancy make it responsive too! But don't go too over the top, people
want to be impressed but no one likes a show off! This is your time to be creative though, your chance to make something
that looks unique, something that doesn't look like a template.

I'm fortunate enough to be able to use a service at university that reviews CVs and the first thing they said before they'd
even read it was "ooo that's nice, very different" which was exactly what I was looking for!

## Print media CSS
This is the important bit! This is where you can make magic happen and show/hide certain things for printing and shuffle things
around to fit better on a couple pages (the 2 page rule still applies on paper, but you can be a bit more flexible with a screen).

For those who don't know, CSS allows you to specify different rules depending on the media your content is being viewed on. There 
are a few different types of media you can specify specific layout for such as TVs, screen readers, mobile devices and of course printers.
More information about print media can be found [here][1].

It's up to you if you want to customise your layout for different screens but at the very least you should make sure your content
looks good when it comes out of a printer. I can almost guarantee someone will want a hard copy of it at some point or someone
will ask you to send them a pdf. (But that doesn't mean it's a wasted effort, I put a line at the bottom of my CV stating it had
been created using HTML and CSS with a link to the online version). To get a pdf version just hit print in Google Chrome and save as pdf.

You may also want to show things on your printed version that you don't want to show on the screen version such as contact
numbers and your address. Conversely you may want to hide any images you display online to save space on the printed version,
you may also want to tweak the font sizes etc. As a quick example below is the printed version of my CV.

<figure class="vspace">
    <img src="/img/posts/dev/2014/08/printed_screenshot.png" alt="printed cv" title="printed cv">
    <figcaption class="dark" style="top: -16px; width: 541px">
        <p>The printed version of my CV</p>
    </figcaption>
</figure>

The CSS to create this printed copy is shown below:

{% highlight javascript %}
    @page {
        size: A4;
        margin-bottom: 30px;
        margin-top: 20px;
    }
    @media print {
        body {
            font-size: 12px;
        }
        nav {
            display: none;
        }
        .printBtn {
            display: none;
        }

        h2 {
            font-size: 20px;
        }
        h4 {
            font-size: 18px;
        }
        h5 {
            font-size: 15px;
        }
        
        .printHide {
            display: none;
        }
        .print-100 {
            width: 100%;
        }
        .print-70 {
            width: 68%;
        }
        .print-up {
            position: relative;
            top: -60px;
        }

        .personals {
            position: relative;
            top: -20px;
            margin-bottom: 0px;
            font-size: 15px;
            height: 50px;
        }
        
        .page {
            float:none;
            page-break-after: avoid;
        }

        #coverPrintPage {
            position:relative;
            top: -60px;
            font-size: 13px;
            page-break-after: auto;
        }

        .inner-page {
            display: block;
            page-break-before: avoid;
            page-break-after: avoid;
        }

        #coverPrintPage hr {
            display: none;
        }

        .showPrint {
            display: block;
        }
        #divAddr {
            text-align: right;
            margin-top: 20px;
        }
        
        #mainContent {
            position: relative;
            top: -55px;
            page-break-before: avoid;
        }

        #coverFooter {
            text-align: center;
        }

        .cvFooter {
            position: relative;
            top: 12px;
            font-size: 8px;
            display: inline;
            page-break-after: avoid;
        }
    }

    @media screen,tv,projection,handheld {
        .personals {
            display: none;
        }
    }

    .personals ul {
        list-style: none;
    }

    .cvImg {
        position: relative;
        top: 20px
    }
{% endhighlight %}

I also added a printable version button to the online version, which in reality just used JavaScript to print the page and then let 
the browser do the right thing with the CSS, so if you clicked the button or pressed print in the browser, you'd get the same result.

## Handling cover letters
If you're sending out CVs you probably want to write some cover letters. This is where something like [Jekyll][2] comes into it's own.
Using templates you can write your cover letters in markdown and then use Jekyll to render it into HTML and
include your CV at the bottom of the page.

Personally I planned to write a cover letter per employer (which actually only turned out to be one) and put each one on a sub directory of the main CV.
I.e. I had /cv and /cv/company1, /cv/company2 ... so that if one of them deleted their company name from the URL (e.g. they went to /cv) they didn't end up with a 404
but at the same time couldn't easily see who else I'd applied for.

Cover letters are relatively easy, just make sure the content fits on one A4 page and you're good to go, you probably want to display it
the same way online as you would on paper, except you'll probably want to include an address and date for the printable version.

## Analytics
Apart from an excuse to show off your skills, one of the nice things about an online CV is you can embed analytics.
This way you can track who's actually read them, has someone spent the time reading your CV, 
or have they just deleted your email? If they've read it, did they read it for a few seconds, or did they spend a reasonable amount of time
reading it?

Personally, I used [CANDDi][3] for this. When I sent out emails, I'd include CANDDi tracking codes in the URL so I could
see who'd clicked through. I was then able to setup alerts so that if others in the company viewed my CV I'd get an email about it.

For example, I had an interview with a company one lunch time around a week after I'd sent them my CV. On the day I emailed it over I could
see them reading it and then coming back to it through the day and I could see them browsing the rest of my site to get a broader view of me (so be careful
what you post). This meant they were doing their homework, which was good, they wouldn't do that level of work if they weren't already
somewhat interested in me?

Over the week between emailing my CV and before my interview CANDDi alerted me on several occasions that various members of the company were on my site
checking me out. Which promoted me to do my homework on the company more thoroughly than I had originally and also go back
and read up on what was on my site in case I was quizzed on it (which sure enough I was).

Finally the morning of my interview CANDDi alerted me again that they were back on my site! I could watch them going around
my site, off to my twitter profile, off to LinkedIn ... again prompting me to do a final bit of homework around the company
and the people who'd be interviewing me! It was also nice to be able to mention the fact I could see them browsing at my interview.

Overall being able to know that people had spent their time researching me meant I could go into the interview much more
confidently because as I said above, they wouldn't of spent that time if they weren't already somewhat interested. As
an excuse to show off your skills online CVs are great, and it was a good exercise to learn more about css media. Just
make sure that if you put your CV online it doesn't look awful when it's printed!


[1]: https://developer.mozilla.org/en-US/docs/Web/Guide/CSS/Media_queries
[2]: http://jekyllrb.com/
[3]: {{site.data.links.canddi}}