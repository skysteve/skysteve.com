---
layout: post
title:  "Detecting DOM changes across browsers"
date:   2014-04-01 22:00:00
category: dev
tags : ["javascript", "dev", "mutation_observer"]
code : true

tease: "Detecting DOM Chances in JavaScript"
summary: "There are times when you need to know when a page changes underneath you. This post outlines 3 ways you can achieve this and still support legacy browsers."
img:
    alt: "Browser Logos"
    style : "width: 100%"
    url: "/img/posts/dev/2014/04/browser-logos.png"
---

## Why detect DOM changes?
There are many reasons why you might want to monitor changes in the DOM. In my case it was part of my job at [CANDDi][1],
it is important to use that we have an up to date view of the DOM so we can track people's activity even if the page changes
under us. But there are many other reasons why you might want to know about changes. For example if you were to build a browser plugin
that did something with tweets when you're on twitter, you'd want to know when new tweets were loaded.

## How to Detect Changes?
As I see it there are 3 ways to detect changes in the DOM depending on your browser/requirements. I'm ignoring [Mutation Events][2]
because they're deprecated, slow, caused browser crashes and are just generally horrible! Which leaves us with:

1. MutationObserver
2. Callbacks
3. Polling

### MutationObserver
MutationObserver is the gold standard when it comes to monitoring DOM changes! **BUT Browser support is IE11+ and older versions
of chrome/safari required a WebkitMutationObserver rather than MutationObserver** (see [Can I Use][3]). If you don't need to support older browsers,
this should be all you need and is very neat. If not, you'll probably want to implement one of/a mix of the 3 methods.

To get started you'll need to define a callback function. This gets called every time the dom changes (depending on what
you're listening to (but more of this in a minute). You can only have one callback per MutationObserver, but one MutationObserver
can observe more than one element changing.
<pre>
{% highlight javascript linenos %}
//setup our callback function to loop through each mutation and print the number of nodes
//that have been added and removed
var fnCallback = function (mutations) {
    mutations.forEach(function (mutation) {
        console.log(mutation.type, "added: " + mutation.addedNodes.length + " nodes");
        console.log(mutation.type, "removed: " + mutation.removedNodes.length + " nodes");
    });
};

//now create our observer and get our target element
var observer = new MutationObserver(fnCallback),
        elTarget = document.querySelector("#divTarget"),
        objConfig = {
            childList: true,
            subtree : true,
            attributes: false,
            characterData : false
        };

//then actually do some observing
observer.observe(elTarget, objConfig);
{% endhighlight %}
</pre>
This sets up a MutationObserver that will simply loop through all the mutations it gets and console log the mutation type
and the number of nodes that have been added and the number that have been removed. You can see this example at [JSFiddle] [4]
(make sure to have developer tools open to see the console.log output).

Notice here we're binding to a specific div target element. That means the browser will only alert us when element change
inside that div. We can do whatever we want to the rest of the page and the browser won't let us know. For a lot of applications
like our twitter example above, this is fine. However there may be cases (such as mine) where you need to know if anything
in the document changes. In this case simply change the observe call to be:

{% highlight javascript %}
observer.observe(document, objConfig);
{% endhighlight %}

Now you'll be told about any changes to the DOM :) Note this also works for [document fragments] [5]. For example below your
callback will be called any time you add/remove any elements to the document fragment.

{% highlight javascript linenos %}
var fragment = document.createDocumentFragment();
observer.observe(fragment, objConfig);
{% endhighlight %}

Let's take a closer look at our options/config object from the example above:

<pre>
{% highlight javascript %}
var objConfig = {
    childList: true,
    subtree : true,
    attributes: false,
    characterData : false
};
{% endhighlight %}
</pre>

Here we're telling the browser we want to be told only when elements are added/removed from the dom (childList : true)
and that we care about child elements changing too (subtree : true). However we're also telling it that we don't want to know
if attributes change or if character data changes. For the example of detecting when new nodes are inserted, it doesn't
matter if attributes change.

However suppose you had a div that you wanted to hide if a certain attribute was added for some reason, your configuration
would likely want to look something like:

<pre>
{% highlight javascript %}
var objConfig = {
    childList: false,
    attributes: true,
    characterData : false
};
{% endhighlight %}
</pre>

Here we tell the browser to only call us when the attributes on our target element change. We don't care if the html inside
it changes, we only care if the attributes change. (This might be attributes added/remove or it might be the value of an
attribute changes). You can also pass the attributeFilter option in the configuration hash to tell the browser you only
care about certain attributes.

<pre>
{% highlight javascript %}
var objConfig = {
    childList: false,
    attributes: true,
    characterData : false,
    attributeFilter : ['style', 'id'],
    attributeOldValue : true
};
{% endhighlight %}
</pre>

Here we tell the browser to only let us know if the style or the id attributes on our target element change and ask it to
pass us the old values when it calls our callback (along with the new values).

I suggest if you're playing around with mutations you adapt the code above and place a debugger; statement in the callback
so you can take a look at the structure of a mutation object and work out how you want to use it.

### Callbacks
Next up we have callbacks. If you're lucky enough to have full control over the page (i.e. you're not writing a library or similar)
you should be able to know when the DOM changes (because you've instigated it). However if you are making a library and you want to
know if the DOM has changed and you're running for example on IE9 which doesn't support MutationObserver what do you do?

One thing you can do is use Mutation Events but as discussed earlier these are horrible and again have limited browser support.
Alternatively you can make a callback mechanism and ask to be called when the dom changes. This is fine, but suppose the developer
using your library didn't read the documentation and doesn't realise he's meant to call your callback, then you're stuck!

BUT! You can be clever! For example if you know the page is using a library like [Mustache] [6] or [Handlebars] [7] you can
override the render method with your own then at least you have a fair idea of what **might be** coming (I say might because you
actually have no idea if this content will end up in the dom). For example using mustache:

<pre>
{% highlight javascript linenos %}
//keep track of the actual mustache render function
var fnExistingRender = Mustache.render;

//override the render function so we know what's going on
Mustache.render = function () {
    var strHTML = fnExistingRender.apply(this, arguments),
        fragment = document.createDocumentFragment(),
        holder   = document.createElement("div");

    //we need a holder div because we can't set innerHTML on a fragment
    holder.innerHTML = strHTML;

    //now add the HTML to the fragment
    fragment.appendChild(holder);

    //does are fragment have any <a> tags? (fragments are fast - the dom is slow)
    var arrAtags = fragment.querySelectorAll('a');

    //if we have some tags great, lets check if they're in the dom in 1/2 a second
    //because at this point they're not!
    if (arrAtags.length > 0) {
        setTimeout(function () {
            var inDomTags = fragment.querySelectorAll('a');
            //do what you need to do (note this will return any existing <a> tags too)
        }, 500);

    }

    return strHTML;
};
{% endhighlight %}
</pre>

Here we override mustache's render method so we can intercept the html returned and analyse it and see if it may be useful.
In this example we're looking for link tags so we could just do if (-1 !== strHTML.indexOf("<a")) but the above
example allows for easy extending for more complicated query selectors. It's worth noting as commented in the code we have
no idea what's going to happen to the HTML string we return, it may end up in the dom, it may be passed back to a server,
it may end up in the console... So we have to set a timeout and give it time to end up in the DOM. Plus if we're wanting
to add event handlers, we need the element that's in the DOM. But if nothing matches, checking the fragment is a quick
way to know we have nothing to do (which in many situations will be the case).

This same technique could be applied to many things other than Mustache templates, for example you could do the same thing
with AJAX requests if you know the page is going to change because of the result of an AJAX request.

### Polling
The final option is the most primitive, slow and is really a last resort for when you can't use MutationObserver and have
no idea what's going on in the DOM or you're unable to override functions for whatever reason. It may also be a fail safe
way of checking something unexpected hasn't happened. The code for this is quite simple:
<pre>
{% highlight javascript linenos %}
setInterval(function () {
    //query the dom for any elements with an attribute class="navItem"
    var arrNavItems = document.querySelectorAll('.navItem');

    //if nothing matched - bail out
    if (!arrNavItems) {
        return;
    }

    //do whatever you need with the items
}, 10000);
{% endhighlight %}
</pre>
Basically what we're doing here is telling the browser to call our function every 10 seconds. In that function we then
check if we have any elements with a class of navItem. And that's it, what you do with the nodes is up to you :) It's
simple code, but relatively expensive. If you can, avoid using it and certainly avoid setting the interval too short,
querying the dom every second will more than likely slow the page down for the end user.


## Conclusion
And that's it! That's my thoughts/experience on observing DOM changes. In reality the code I'm using in production is a
bit more heavy duty and fail safe than the examples above, but they should be a good starter. Any queries/suggestions
please drop me a comment below.

[1]: {{site.data.links.canddi}}
[2]: https://developer.mozilla.org/en-US/docs/Web/Guide/Events/Mutation_events
[3]: http://caniuse.com/mutationobserver
[4]: http://jsfiddle.net/skysteve/v465D/3/
[5]: https://developer.mozilla.org/en-US/docs/Web/API/DocumentFragment
[6]: http://mustache.github.io/
[7]: http://handlebarsjs.com/