---
layout: post
title:  "Life with LightwaveRF"
date:   2014-01-11 15:15:00
category: random
tags : ["lightwave", "random"]

tease: "LightwaveRF a year on"
summary: "It's been around a year since I first installed LightwaveRF in my flat. This post is basically an overview
of LightwaveRF as I see it."

img:
    alt: "LightwaveRF Wifi Link and original mobile app"
    caption: "LightwaveRF Wifi Link and original mobile app"
    url: "http://www.lightwaverf.co.uk/images/D/JSJS%20LW500%20WH.jpg"
---

Let me start by saying LightwaveRF is awesome! If you're considering home automation in the UK I'd highly recommend it. (If fact I did and now my boss has upgraded his home too!)

As a techy person growing up I'd see home automation on TV countless times but it always seemed expensive and years
away from an everyday consumer product. However a couple of months after moving into my first home I'd
bought all the important things like sofas and TVs and so started investigating automating my flat.

I couldn't tell you how I actually found out about LightwaveRF, it was either through [B&Q][1] or the [LightwaveRF site][2] after some late night Googling. Either way I remember going to bed thinking about it trying to talk myself
out of spending more money but the next day I took a trip to B&Q to buy a new light switch for my livingroom.

One of the great things about LightwaveRF is you can build it up from just one switch. I started with just a light
switch but within a matter of days I'd upgraded all the wall sockets in my living room too. And a few months later almost every socket and light switch was a LightwaveRF switch. (All my heating is electric so I haven't tried out the heating range)

#### Installation
Installing the devices is really really simple! As long as you feel safe and confident doing so,
it's simply a case of unscrewing your existing sockets/switches and screwing the LightwaveRF ones in place. (Making sure you kill the mains power before doing so).
Then it's just a case of pairing the devices (details of which can be found [here][3]) but is incredibly simple and
quick. It's basically just pressing the on/off button and pressing a button in the app. After that you're ready to
rock!

#### Apps etc
LightwaveRF can be controlled in a number of ways, either by a physical wall switch (as is traditional),  remote
control, a smartphone app (Android/IOs), Web App, desktop app or Sensors. (There's also a developer API but that's another blog post)
The smartphone app, web app and desktop app only work if you have the [wifi link][4] though.

Personally I couldn't find much use for the sensors, they tie directly to switches rather than talking to the wifi
link which means you can't kick of sequences (more on that later) which seems a shame. I also couldn't find much use
for the desktop app (which is an Adobe air version of the web app) considering I spend my of my time at home on a
chromebook.

I mostly interact with LightwaveRF though a physical remote control, smartphone app or (my custom) web app. The
physical remote is by far the quickest and easiest way to turn on/off lights and sockets, but is limited to just that. But rather than getting your phone, unlocking it and opening the app, the remote is just there.

That said, if you're on your phone already it offers more features and arguably a nicer interface. One thing to say here is that the new mobile app looks MUCH nicer than it did this time last year! That said, by the time it arrived I'd already written my own so I tend to use that over the standard app. For that reason I'll not go into detail here. The same goes for the web app. Again there's a whole other blog post there.

#### Features
LightwaveRF allows you not only to turn on/off devices but also set dim levels for lights, add timers, sequences and moods. Again personally I haven't found much use for moods. Sequences however are very useful. I have a number set up for various things. By far the most common one I use is the "ALL OFF" sequence which cycles through every room in my flat turning all my devices off when I go to bed or leave the flat.

There is also a really handy lock feature. This is perfect for things you want to always be on such as wifi routers
and of course the wifi link itself. It's also really useful if you have small children and would rather they didn't
turn the hot iron on etc. (though not something I have worry about!)

I also run a [sonos][5] system across my flat. I therefore have a sequence setup to turn all of my sonos system on together. Coupled with this is a timer which turns the system on 5 minutes before the alarm set on sonos (to give it time to wake up). Which means when I come to wake up sonos is already playing music across my flat, including the shower room :) And then when I come to leave the flat I run the all off sequence to make sure everything is turned off while I'm at work. (Actually all off is run automatically but that's some custom code so I'll skip that for now)

Finally you can also attach an energy monitor to the system which enables you to see how much energy you used today, yesterday, the maximum power usage today and the current usage. This add on is only around £25 and also very easy to install, just clip it around your mains cable and pair with the wifi link.

There are also a few other features I don't use like moods and sensors but I'll skip these as I don't really know
enough about them. In Short, moods allow you to "save" a room's "state" such as lighting levels and if the TV is on
and then "recall" this state. Sensors do pretty much as you'd expect, detect when you enter a room and trigger a
device.

#### The Negatives
I'd be lying if I said there was nothing wrong with LightwaveRF. These fall into a couple of different categories, hardware faults/limitations, software limitations. None of these are what I class as a deal breaker, but just things to be aware of.

Lets start with the hardware. First thing to note is in the past year I've had 2 sockets which have locked themselves into an on state. I tried everything from unlocking them to killing the mains power to the socket, but ended up taking them back to be replaced. (They were replaced for free so this wasn't a major problem).

Next there's the lack of fused switches (the kind you find to turn your oven on/off or electric radiators). Again this isn't a huge issue, but is somewhat annoying when you have electric radiators and can't program them to turn on in time for your return from work. You can buy relay switches if you know what you're doing, but it would be nice if they were a standard switch.

Also worth noting is the light switches don't just go from on to off, they actually gradually (over around 1 second)
bring the dim level up from 0 to whatever it was when you turned the light off. So you'll need dimmable light bulbs.
This means most cheap energy efficient light bulbs won't work which means you may need to buy some new light bulbs too.

Software wise, as I've previously said I use my own custom code so here are my thoughts on what I have used. To start with the web app is horrible! Built in flash, it's slow and ugly! Feature wise it works, but it's just horrible to use.

Mobile wise, the new app does look much nicer than the old one, but I still feel there are some awesome features missing. Some of which I've included in my app. The key ones here are NFC and Geo fences. With NFC tags I'm able scan a tag next to my bed which triggers a sequence to turn everything off. Geo fence wise, when I get within 200 meters of my house all my sonos kit turns on ready for my arrival. The annoying thing here is that both of these were really simple to do and add so much to the system!

Also on the mobile front, I believe there is now a setting to talk to a central server or directly to your wifi link
(if you're on the same network). When I started using the system there was no option for this and there was often a
noticeable delay between sending a command and the device actually turning on/of because of the delay talking to the
central server. Talking directly to the wifi link is almost instant though which is nice.

#### The Positives
Enough with the negatives, as I said at the start, the system is awesome and runs across most of my flat. One of the big draws for me was it was relatively cheap and easy to install. And you can try it out with one or two sockets and build it up over time!

The system makes it very easy to group your sockets by room making it easy to turn all the devices in one room off in
 one go. As I mentioned earlier sequences make it very easy to turn a group of devices on together. They also work
 well if you want to use them as security lighting if you're out but want people to think there's someone in.

The sockets themselves are very well built and in my opinion look great! There's a choice of colours such as white,
black nickel, chrome (and more) and all the faces clip on to hide the screws. Each device does have 2 LEDs,
orange (off) and blue (on) which let you know what the device is doing, and also makes your house look a bit
futuristic!

From a green energy point of view it makes being green very easy! If you're out and realise you left the light on,
on worries, turn it off! If you go to bed and forgot to turn the TV off standby, open the app and turn it off! If you
 get the energy monitor too it'll let you know how much energy you're currently using along with how much you used in
 total yesterday and today.

#### Closing Words
If you're considering home automation I urge you to consider LightwaveRF, not only is it cheap and easy to install, you can build it up over time, so start with one or two switches, if it's not for you at least you haven't wasted too much on it.

  [1]: http://search.diy.com/search#w=lightwaveRF
  [2]: http://www.lightwaverf.com/
  [3]: http://www.lightwaverf.com/technical-help
  [4]: http://www.lightwaverf.com/store/home-automation-controls/smart-homes-wifi-link
  [5]: http://www.sonos.com
