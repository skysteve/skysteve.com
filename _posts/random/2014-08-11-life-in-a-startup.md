---
layout: post
title:  "Life in a startup as a graduate developer"
date:   2014-08-11 13:00:00
category: random

tease: "The highs and lows of startup life"
summary: "Startups offer many opportunities and challenges that you can't find in any other type of business! I'd recommend them to anyone!"

img:
    alt: "CANDDi logo"
    caption: ""
    styleCaption: "display:none"
    url: "/img/home/canddi.png"
---

As a final year computer science student at some point you'll almost certainly find yourself looking for jobs for when you
graduate. Chances are top of your list are companies like Google, Facebook, Apple, Microsoft... These massive companies are
the ones you hear about all the time and tend to be the first place you think of when you need a developer job.

Some people might think of some "smaller" companies like Dropbox, Spotify... because you occasionally hear about what it's
like to work for these companies. They may offer any number of the following in their offices: sofas, games rooms, ping pong tables,
free food... all of which sounds awesome but then you find out the only jobs are in Germany.

Very few graduates will look at the "real" startups, local companies who only employ a handful of people and who's development
team is only 2-3 people. Chances are you've never heard of most of these companies but depending on your location there's likely to be
a few of them around.

Take Newcastle for example, it now has a space called [Campus North][1] which houses approximately 30 startups (at time of writing).
Most of these companies are only 1-2 years old and still very much growing rapidly. As a result many of them are always on the lookout
for developers in one way or another. The problem is many of them don't have the time to spend actively recruiting and instead rely on
talking to people at meetups or through word of mouth. (I've put some tips for finding a job in a startup at the bottom of the page.)

## My time in a startup as a graduate developer
This post is meant to be about working for a startup rather than personal experiences but it's helpful to use my story in places.
When I finished university I wanted to go and start my own company, as a result I didn't apply for all the big jobs (maybe a couple just in case). I'd
spent a placement year working for Hewlett Packard so had some experience of big business, but ultimately wanted to do my own thing.
However by the time September came around I realised I had no money to go it alone and my choices were to find a job, or move back in with parents.

Fortunately, one of my lecturers from university put me in touch with a company called [CANDDi][2] which is based in Manchester but had
an office in Newcastle. This seemed ideal, I could stay in the city I'd been to uni in and work for a small company and learn what it took
to start my own. I obviously had never heard of the company, but after a bit of research I decided it sounded like a fun challenge with some
very smart people.

Having been through the normal job interviews I started in early September 2012. Initially the dev team consisted of one person
in Newcastle, one in Manchester and our CEO who did development when he had time (and then me).

Very quickly I learnt that despite my degree I knew relatively nothing about life as a developer! But rather than going into a structured
graduate program like I would of done at one of the larger companies I mentioned earlier, I was thrust straight in a the deep end. Within
the first couple of weeks I was tinkering with the live website (and accidentally took it offline for a period), something that just
wouldn't happen in a larger organisation.

A small company doesn't have the money or the resources to spend sending you on training courses or giving you small projects to do,
they want you working on the live codebase as fast and as effectively as possible! Again something most large companies wouldn't
dream of doing with new graduates. I had to learn very quickly how to understand a large, complex code base (something I was never taught
at university) and how to make changes to that codebase in a way that the rest of the team could understand and maintain.

By default I was the JavaScript/Frontend developer at CANDDi, there was also a sys-admin and a php/backend developer. But in reality
we all dipped in to each other's areas when necessary. Which meant my linux skills had to improve rapidly and I had to try and understand/write
PHP for the first time. It also meant I ended up with access to our live AWS infrastructure within a few months! Again something many large
companies would restrict heavily but ultimately something that made me feel trusted and valued.

But it's not just development, in a small company you have exposure to almost all areas of the company. For me this meant seeing
how our sales team worked, working with our support team and even our cash-flow and how we could raise investment. Coupled with working in a
co-working environment full of startups (Campus North) I was able to learn a huge about about the highs and lows of small companies and what it really
means to start your own company.

Looking back on that new found knowledge I'm actually rather glad I didn't try to start my own company! I know now that I didn't have anywhere
near the right level of skills and would probably have made every mistake in the book. To cut a long story short, if you have any intentions
of starting a tech company after university, I'd advice spending at least a few months working for a startup first or at least in a co-working
space full of statups. If nothing else it'll give you a lot of connections for later in life!

To cut a long story short, in the (almost) 2 years I was at CANDDi I feel like I've grown hugely both as a developer but also as a person.
I've had the chance to learn a huge amount in a very short space of time. I've been trusted with things some graduates can only dream of
and had a chance to make my own mistakes and learn from them. I've met some amazing people and been part of an awesome little community.
I can honestly say I've had an epic time!

In the past few of weeks I've sadly come to the realisation that there's not much left for me to learn inside CANDDi. As the only
JavaScript person on the team I feel like I've learnt as much as I can on my own and it's time to try working for a larger company
with more experienced developers that I can learn from. But don't get me wrong, I've loved my time in the startup community and I'll
almost certainly be back! 

I'd encourage everyone finishing university to give startups a go, you might find their not for you, you might love them, but you'll
almost certainly be given more freedom and responsibilities than you'd get in a similar corporate job. But not only when it comes to technology,
startups tend to be quite relaxed when it comes to working hours. As long as the work gets done, most people won't mind if you work 9-5, 11-7 or 6-3,
as long as you're there when needed and the work gets done. That said, there are times when you'll need to stay late! If an important customer is going
to leave because something is broken, you'll have to fix it. If a client says they'll sign up if you add another feature by next week, you'll have a few
late nights to make it happen. That's the trade off you make, some days you can wake up later and work from home, other days you'll be in the office
until 10-11pm (with no extra pay).

Having said that, they money startups pay is generally reasonable, certainly I was offered more money in a startup than I would of been for other local
tech companies in the area. Performance reviews tend to happen in coffee shops or pubs as and when your boss feels like you've earned them rather than
every 12 months like in more structured companies.

There's also a great deal of community spirit around startups, certainly there was where I was. Campus North is filled with many startups
all going through different phases of growth and everyone in there knew they could ask anyone for help at any time about anything! I lost count of the
number of times random discussions over lunch lead to me solving a problem I'd been stuck on all morning. Outside of work too
there's many events and people to go out with (more on this later).

Startup life is brilliant! Yes there are times you work late and wonder if it's worth the pain, but then other times you wake up and remember
how lucky you are to be part of such a special community and work with amazing people. As I write this looking back on what I've left behind
it does genuinely make me realise how lucky I've been. I know I'll be back in a few years with more experience and knowledge, hopefully with
a company of my own!

## Disadvantages of startups
I'd be lying if I said startups were perfect, their not, no job is. Because of their size startups offer their own set of things
you should be aware of. (some of these may vary depending on the company)

* With great power comes great responsibility!
    * You'll have the power to break some very important things, but YOU will be expected fix them
    * Things that are broken become very important, if you break something at 5:00, expect to be there until it's fixed, even if that means 11:00
* There's no one else to call - in a larger team you can ask for help, in a startup where you're "the JavaScript guy", you won't get much help from "the PHP guy"
* You might work long hours - as above, if something needs to ship to get a new customer, you might need to put the extra hours in to make it happen
* Little/no benefits - don't expect health care, pension schemes....
* Reduced job security
    * With it being a small company there's always a chance it can fail and go out of business leaving you out of a job
    * If you're not performing in the first few months, you might be asked to leave - a small company doesn't have the money to spend to train you for long periods
* Self taught - a lot of the time you'll have to teach yourself rather than having people to go to or training courses
* Nowhere to hide - with it being a small team, if you're not pulling your weight it's very easy for everyone else to spot!

## Advantages of startups
Having said all that, there's loads of reasons why working in a startup is great! (some of these may vary depending on the company)

* Lots of power/responsibility - as mentioned above you'll likely have more access than you'd ever get in a larger company
* Amazing learning opportunities - not only do you get to see your area of work but you get an insight into all already of the company
* Less management - startups don't tend to bother with management meetings, instead they prefer to go to the pub and talk
* Less structured reviews - rather than annual pay reviews, you might find you boss offers you a pay rise more frequently because they can directly see the contributions you're making
* More relaxed atmosphere
    * No dress code
    * Flexible hours
    * Working from home
    * sofas/comfy communal areas
* Enhanced team spirit - while we had a sales team/dev team... in reality we all win/loose together which makes everyone closer
* Friendly - as above, your boss will likely take everyone to the pub rather than the meeting room so you become more friends than colleagues
* Share options - depending on the company you may be offered shares in the business, if the business is bought out, you can end up rich!
* Cutting edge technology - most startups use modern frameworks, platforms, services... so you're always at the cutting edge of tech

## Finding a job in a startup
As above, finding a job in a startup can be harder than finding a job in a larger company. The main challenge is finding
something you're interested in while not knowing any small companies in your area. This is where meetups are great! Many
areas run events targeted at developers. This usually occur in the evenings although you get the odd event over a weekend.

Many of these meetups are free to attend and (depending on your area) tend to cover a wide range of topics. For example in Newcastle
there's meetups for JavaScript developers, PHP developers, Android developers, AWS sys-admins and many many more. These
meetups are generally attended by a wide range of developers from recent graduates to full time developers for both small and 
large companies looking to share and expand their knowledge.

These meetups are a great place to get chatting to local developers to find out who's around in your area and what jobs
they may have available. It may even be a case that you impress them so much they make a job available for you that wasn't
there to start with! As you get more familiar with the community people you've never met before may approach you because
they've heard good things about you.

Startups struggle to recruit good developers so if you're good at what you do and you have enough connections, finding a job is
easy! But beware the opposite is true, if you're bad at what you do, word will spread very quickly but don't worry too much,
everyone expects recent graduates to be somewhere in the middle. As long as you can learn fast and adapt you'll have no problem
getting a job and fitting in!

## Summary
I hope I've managed to shed a bit of light on what it's like working in a small company and what an amazing experience it can be.
I've tried not to focus too much on my own experiences (maybe that'll be a future post) but instead give a broader overview
of what working in a startup can offer a recent graduate. I thoroughly enjoyed my time in the startup community and would recommend
it to anyone! I know I'll be back after a few years more experience/learning and already I can't wait!

[1]: http://ignite100.com/
[2]: {{site.data.links.canddi}}