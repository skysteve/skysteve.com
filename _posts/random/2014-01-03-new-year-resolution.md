---
layout: post
title:  "Just do it! 2014's resolution"
date:   2014-01-03 15:15:00
category: random
tags : ["resolution", "random"]

tease: "2014, The year of doing!"
summary: For 2014 I decided it was pointless to make a resolution to eat less chocolate, get fitter ... <br/> Instead I decided "Just Do It" was much a much better thing to aim for and he's why.

img:
    alt: "Just Do it!"
    caption: 'Image taken from <a href="http://www.wallsave.com/wallpaper/1920x1200/nike-free-just-do-it-x-images-760819.html" target="blank">
    http://www.wallsave.com/</a>'
    url: "/img/posts/random/2014/01/justDoIt.jpg"


---
In the past I always saw myself as a very decisive and as a result very active person. I'd often get in from school
and be out paragliding as soon as I could. Even if the weather was marginal I'd drive out on the off chance it was
flyable. But a few weeks ago I noticed I'd become much more cautious lately.

At what point in my life this shift happened I can't pin point, I suspect it's been a gradual change since I moved
away to uni and began having responsibilities and developed further when I graduated and got my own place and a
full time job.

But the timing doesn't really matter, the point is I've become more cautious and less "up" for doing things. I guess
this is what I've been warned about for years by people saying "kids have no fear" etc. I'd never really believed it,
 but apparently it's true!

The problem is I liked living life on the edge, I loved being out doing things but it seemed to be slowely slipping
away. Sure I've taken up kitesurfing recently and try to get out as much as possible and I still fly from time to
time. But I know for a fact there have been days when I've done things around the house/town when other people risked
 the 2 hour drive to go flying and had an epic day!

So around early December I decided to try to force myself back to my old ways. I thought come 2014 I'll make it my
resolution to keep "just do it" in my head. When I'm faced with a situation where I can't really be bothered,
I'll force myself into it.

And I have to say so far so good. It's only one example but on New year's day back home in the Isle of Man there's
somewhat of a tradition to go swimming in the sea. I'd done it a couple of years ago but missed it last year because
I was in Newcastle. This year the weather was horrible, it rained and the wind was strong and cold!

I woke up thinking "No, not today. It's not worth it." But then I told myself if I can't make myself do this on the
first day
of the year what hope do I have for the rest of the year!? So I drove down to the beach and stood around in the cold
waiting to go in before running into the freezing cold sea. Yes it was cold, but in a weird way it was fun! And most
importantly I enjoyed myself and was glad I'd made myself do it.

The point here isn't the fact I swam in the sea on new year's day. It's that I forced myself to do something that
deep down I wanted to do, but that equally I couldn't really be bothered to do. But that as a result I'd enjoyed
myself! So looking forwards 2014 will hopefully be a year of accomplishment! The big goal this year as part of "just
do it" is to finally go cross country paragliding in the UK!


(Additionally this is my first proper blog post outside of work, and my second ever blog post including work!)