---
layout: post
title:  "BBC playlister in Rdio"
date:   2015-06-25 22:00:00
category: random

tease: "Importing BBC playlister to Rdio"
summary: "BBC playlister is a great service for finding new music and for consuming pre-made playlists. However the service normally
only lets you listen to clips of tracks with the option to export to Spotify or Deezer. Great if you're a spotify/Deezer user but
what about Rdio?"

img:
    alt: "BBC playlister page"
    caption: "The home page of Rdio"
    url: "/img/posts/random/2015/06/playlister.png"
---

## Update
Rdio has now been shut down and is no longer available. Parts of Rdio have been bought by pandora.

### TLDR

* Create a free [Spotify account][5] *<small>For personal playlists</small>
* Create/find a playlist on [playlister][2]
* Export the playlist to spotify
* Copy the URL to [re/spin][4]
* Follow the steps in resp/in
* Enjoy your new playlist on rdio

<hr>

I've already written a post explaining [why I love Rdio][1]. Though that post is slightly out of date since Echo nest has
been bought by spotify and as a result no long a part of Rdio, but the service is still awesome. Obviously when it comes
to streaming services a lot of it is personal preference and each service has it's positives and negatives,
but that's not really the point of this article.

### What is BBC playlister?
[BBC playlister][2] is a music discovery tool from the BBC designed to help you discover new music from across the BBCs
various music channels including [Radio 1, Radio 2][7] and [BBC introducing][3] (Their unsigned artists project). You can also
find music you heard on it's TV shows. You can also listen to pre-made playlists complied by radio DJs or based on themes.

Playlister is great for discovering new music especially if you know you like the music a certain DJ plays or want to find
that song you heard on the TV/radio earlier. It's also great for finding tracks you've forgotten about in playlists such
as [One Hit Wonders of the Millenium!][6]. Now that echo nest have retired their discovery playlists,
the BBC playlister is starting to play a bigger role in my music discovery, especially
as I find myself listening to the radio less and less lately.

### Exporting Playlists
Once you've created a playlist or find a pre-made playlist you'd like to hear in full there's an option to export it to
Deezer or Spotify. If you click on the export to spotify button, a spotify tab will open in your browser. At the top
you'll see the playlist url which you can copy. The URL should look something like this: https://play.spotify.com/user/bbc_playlister/playlist/37BaKRvEBrp6YEuo7StkDM

Then head over to [re/spin][4]. If you've used the service before it should be fairly straightforwards. If not, you
will need to authorize it with your Rdio account first to allow it to create your playlists.

Once you're signed in, you'll see a text box that says "by Spotify playlist URL". Paste your playlist's url in there then click submit at the bottom.
It will take a seconds while it searches for songs. Once finished it will show you what it's found and give you the option to
create your playlist. Click the blue button at the top and it'll make a playlist in rdio and start adding the songs. Tracks will
"go blue" as they're added and the page will turn blue when it's done. (Songs shown in grey are currently unavailable on Rdio).

That's it! Job done! If you go to rdio you should now see your new playlist on the left with all your other playlists.

### Gotchas
There are a few thing to know when exporting playlists though.

The first one is is quite a big one! The re/spin service seems to choose the first match it can find and sometimes that means
it'll choose a song that's not available in your country. Or sometimes there's also licencing issues which means a certain track
isn't on rdio at the time and it'll match a "tribute" song. Depending on your tastes this may be fine, personally I hate the
tribute songs and remove them when I spot them. As a general rule I find that about 90-95% of a playlist will be correct
with the rest either missing/tribute acts/unavailable. Sometimes you can fix these tracks manually, sometimes they're just not there.

Playlists updated in playlister won't automatically get updated in rdio. If you want to keep up to date with a playlist that
changes over time you'll need to repeat the re/spin import process again.

You will need a free spotify account to be able to export custom playlists. If you don't have one you can [sign up here][5].
This is just so BBC playlister can export songs into a spotify playlist for you. If you just want to use pre-compiled playlists
you don't need a spotify account.

It would be great if this process was a little more streamlined and hopefully the playlister team will add support for rdio exports
but for now it's better than nothing. Especially now that Rdio has broken all it's ties with echo nest and the echo nest discovery
playlists have been retired.

[1]: /random/2014/05/26/rdio.html
[2]: www.bbc.co.uk/music/playlister
[3]: http://bbc.co.uk/introducing
[4]: http://resp.in/
[5]: https://www.spotify.com/us/signup/
[6]: http://www.bbc.co.uk/music/playlists/zzzzfj
[7]: http://www.bbc.co.uk/music/tracks/recent